\contentsline {chapter}{\numberline {1}Network Building Blocks}{3}{chapter.1}% 
\contentsline {section}{\numberline {1.1}Nodes}{3}{section.1.1}% 
\contentsline {section}{\numberline {1.2}Edges}{3}{section.1.2}% 
\contentsline {subsection}{\numberline {1.2.1}Weight}{3}{subsection.1.2.1}% 
\contentsline {subsection}{\numberline {1.2.2}Direction}{3}{subsection.1.2.2}% 
\contentsline {section}{\numberline {1.3}Paths}{4}{section.1.3}% 
\contentsline {subsection}{\numberline {1.3.1}Geodesic Distance}{4}{subsection.1.3.1}% 
\contentsline {subsection}{\numberline {1.3.2}Eccentricity of a node}{4}{subsection.1.3.2}% 
\contentsline {subsection}{\numberline {1.3.3}Diameter of a network}{4}{subsection.1.3.3}% 
\contentsline {subsection}{\numberline {1.3.4}Radius of a network}{4}{subsection.1.3.4}% 
\contentsline {section}{\numberline {1.4}Adjacency Matrices}{4}{section.1.4}% 
\contentsline {section}{\numberline {1.5}Degree, Degree Distributions}{5}{section.1.5}% 
\contentsline {chapter}{\numberline {2}Network Structure}{6}{chapter.2}% 
\contentsline {section}{\numberline {2.1}Motifs}{6}{section.2.1}% 
\contentsline {subsubsection}{Slide Detail}{6}{section*.2}% 
\contentsline {section}{\numberline {2.2}Cliques}{6}{section.2.2}% 
\contentsline {section}{\numberline {2.3}Hubs and Authorities}{6}{section.2.3}% 
\contentsline {section}{\numberline {2.4}Graph Components}{7}{section.2.4}% 
\contentsline {subsection}{\numberline {2.4.1}Clustering and Clustering Coefficient}{7}{subsection.2.4.1}% 
\contentsline {subsubsection}{Density}{7}{section*.3}% 
\contentsline {subsection}{\numberline {2.4.2}Assortativity and Dissortativity}{7}{subsection.2.4.2}% 
\contentsline {subsubsection}{Modularity}{7}{section*.4}% 
\contentsline {section}{\numberline {2.5}Bridges}{8}{section.2.5}% 
\contentsline {chapter}{\numberline {3}Centrality}{9}{chapter.3}% 
\contentsline {section}{\numberline {3.1}Degree Centrality}{9}{section.3.1}% 
\contentsline {section}{\numberline {3.2}Eigenvector Centrality}{10}{section.3.2}% 
\contentsline {section}{\numberline {3.3}Closeness Centrality}{10}{section.3.3}% 
\contentsline {section}{\numberline {3.4}Betweenness Centrality}{11}{section.3.4}% 
\contentsline {chapter}{\numberline {4}Network Archetypes}{12}{chapter.4}% 
\contentsline {section}{\numberline {4.1}Regular}{13}{section.4.1}% 
\contentsline {section}{\numberline {4.2}Random}{13}{section.4.2}% 
\contentsline {section}{\numberline {4.3}Small-World}{14}{section.4.3}% 
\contentsline {section}{\numberline {4.4}Scale-Free}{15}{section.4.4}% 
\contentsline {section}{\numberline {4.5}Affiliate or Bipartite Networks}{16}{section.4.5}% 
\contentsline {section}{\numberline {4.6}Core-Periphery Structure}{17}{section.4.6}% 
\contentsline {chapter}{\numberline {5}Other Networks Concepts}{18}{chapter.5}% 
\contentsline {section}{\numberline {5.1}Homophily}{18}{section.5.1}% 
\contentsline {subsection}{\numberline {5.1.1}Social Distance}{18}{subsection.5.1.1}% 
\contentsline {section}{\numberline {5.2}Cohesion}{19}{section.5.2}% 
\contentsline {section}{\numberline {5.3}Consolidation}{19}{section.5.3}% 
\contentsline {section}{\numberline {5.4}Preferential attachment}{20}{section.5.4}% 
\contentsline {section}{\numberline {5.5}Triadic closure}{20}{section.5.5}% 
\contentsline {section}{\numberline {5.6}Weak Ties}{20}{section.5.6}% 
